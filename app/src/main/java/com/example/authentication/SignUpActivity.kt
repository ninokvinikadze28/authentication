package com.example.authentication

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log.d
import android.view.View
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import kotlinx.android.synthetic.main.activity_sign_up.*

class SignUpActivity : AppCompatActivity() {

    private lateinit var auth: FirebaseAuth
    private val emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_up)
        init()
    }
    private fun init(){
        auth = Firebase.auth
        signUpButton.setOnClickListener {
            signUp()
        }

    }
    private fun signUp(){

        val email = emailEditText.text.toString()
        val password = passwordEditText.text.toString()
        val repeatPassword = repeatPasswordEditText.text.toString()
        if(email.isNotEmpty() && password.isNotEmpty() && repeatPassword.isNotEmpty()){
            if(password == repeatPassword){
                if(email.matches(emailPattern.toRegex())){

                }else{
                    Toast.makeText(this,"Email format is not Correct",Toast.LENGTH_SHORT).show()
                }
                progressBar.visibility= View.VISIBLE
                signUpButton.isClickable = false
                auth.createUserWithEmailAndPassword(email, password)
                    .addOnCompleteListener(this) { task ->
                        progressBar.visibility = View.GONE
                        signUpButton.isClickable=true
                        if (task.isSuccessful) {
                            // Sign in success, update UI with the signed-in user's information
                            d("signUp", "createUserWithEmail:success")
                            Toast.makeText(this,"SignUp is success",Toast.LENGTH_SHORT).show()
                            val user = auth.currentUser
                        }
                        else {
                            // If sign in fails, display a message to the user.
                            d("signUp", "createUserWithEmail:failure", task.exception)
                            val show: Any = Toast.makeText(baseContext, task.exception.toString(),
                                    Toast.LENGTH_SHORT).show()

                        }
                    }

            }else{
                Toast.makeText(this,"Passwords don't match!",Toast.LENGTH_SHORT).show()
            }

    }else{
            Toast.makeText(this,"Please fill all fields!",Toast.LENGTH_SHORT).show()
        }
}

}



